package org.weiyonghua.dao.mogodb;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.weiyonghua.Application;
import org.weiyonghua.domain.User;

/**
 * Created by weiyonghua on 2017/3/27.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class UserRepositoryTest {
    @Autowired
    UserRepository userRepository;

    @Test
    public void test(){
        userRepository.save(User.builder().username("weiyonghua1").password("1245215").build());
        userRepository.save(User.builder().username("weiyonghua2").password("1245215").build());
        userRepository.save(User.builder().username("weiyonghua3").password("1245215").build());
        User weiyonghua1 = userRepository.findByUsername("weiyonghua1");
        Assert.assertEquals(3, userRepository.findAll().size());

        userRepository.delete(weiyonghua1);
        Assert.assertEquals(2, userRepository.findAll().size());
    }
}
