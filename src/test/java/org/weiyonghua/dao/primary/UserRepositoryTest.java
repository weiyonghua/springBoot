package org.weiyonghua.dao.primary;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.weiyonghua.Application;
import org.weiyonghua.domain.User;

/**
 * Created by weiyonghua on 2017/3/23.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class UserRepositoryTest {
    @Autowired
    UserRepository userRepository;

    @Before
    public void setUp(){
        userRepository.deleteAll();
    }

    @Test
    public void findUserTest(){
        User user = User.builder()
                .username("weiyonghua")
                .password("123456")
                .build();
        User saveUser = userRepository.save(user);
        userRepository.findByUsername("weiyonghua");
    }

}
