package org.weiyonghua.dao.secondary;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.Assert;
import org.weiyonghua.Application;
import org.weiyonghua.domain.Message;

/**
 * Created by weiyonghua on 2017/3/27.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
public class MessageRepositoryTest {
    private Logger logger = LoggerFactory.getLogger(MessageRepositoryTest.class);
    @Autowired
    MessageRepository messageRepository;

    @Test
    public void saveTest(){
        logger.info("message save test Start");
        Message message = messageRepository.save(Message.builder().messageContent("这是一条测试消息！").build());
        Assert.notNull(message);
        logger.info("message save test End");
    }
}
