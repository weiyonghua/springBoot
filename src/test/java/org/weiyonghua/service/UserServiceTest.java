package org.weiyonghua.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.weiyonghua.Application;
import org.weiyonghua.domain.User;

/**
 * Created by weiyonghua on 2017/3/23.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
public class UserServiceTest {
    @Autowired
    private UserService userSerivce;

    @Before
    public void setUp() {
        // 准备，清空user表
        userSerivce.deleteAllUsers();
    }

    @Test
    public void saveUserTest() {
        User user = User.builder()
                .username("weiyonghua")
                .password("123456")
                .build();
        this.userSerivce.saveUser(user);
        User userByUsername = this.userSerivce.getUserByUsername(user.getUsername());
        Assert.assertNotNull(userByUsername);
    }

    @Test
    public void deleteByUsernameTest() {
        User user = User.builder()
                .username("weiyonghua")
                .password("123456")
                .build();
        this.userSerivce.saveUser(user);
        this.userSerivce.deleteByUsername("weiyonghua");
        User userByUsername = this.userSerivce.getUserByUsername(user.getUsername());
        Assert.assertNull(userByUsername);
    }


}
