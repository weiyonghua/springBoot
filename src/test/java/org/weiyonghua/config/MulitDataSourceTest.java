package org.weiyonghua.config;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.weiyonghua.Application;

/**
 * Created by weiyonghua on 2017/3/23.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class MulitDataSourceTest {
    @Autowired
    @Qualifier("primaryJdbcTemplate")
    protected JdbcTemplate jdbcTemplate1;
    @Autowired

    @Qualifier("secondaryJdbcTemplate")
    protected JdbcTemplate jdbcTemplate2;

    @Before
    public void setUp() {
        jdbcTemplate1.update("DELETE  FROM  t_user ");
        jdbcTemplate2.update("DELETE  FROM  t_user ");
    }

    @Test
    public void test() throws Exception {
        // 往第一个数据源中插入两条数据
        jdbcTemplate1.update("INSERT INTO `t_user` (`username`, `password`) VALUES (?, ?)", "weiyonghua1", "aaa");
        jdbcTemplate1.update("INSERT INTO `t_user` (`username`, `password`) VALUES (?, ?)", "weiyonghua2", "bbb");
        // 往第二个数据源中插入一条数据，若插入的是第一个数据源，则会主键冲突报错
        jdbcTemplate2.update("INSERT INTO `t_user` (`username`, `password`) VALUES (?, ?)", "weiyonghua1", "aaa");
        // 查一下第一个数据源中是否有两条数据，验证插入是否成功
        Assert.assertEquals("2", jdbcTemplate1.queryForObject("select count(1) from t_user", String.class));
        // 查一下第一个数据源中是否有两条数据，验证插入是否成功
        Assert.assertEquals("1", jdbcTemplate2.queryForObject("select count(1) from t_user", String.class));
    }
}
