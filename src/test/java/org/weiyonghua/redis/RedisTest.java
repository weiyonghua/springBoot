package org.weiyonghua.redis;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.weiyonghua.Application;
import org.weiyonghua.domain.User;

/**
 * Created by weiyonghua on 2017/3/27.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class RedisTest {
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate<String, User> redisTemplate;

    @Test
    public void redisTest(){
//        stringRedisTemplate.opsForValue().set("testKey", "testValue", 5000);
//        stringRedisTemplate.opsForHash().put("testHash", "1", "weiyonghua");
//        stringRedisTemplate.expire("testKey", 5L, TimeUnit.SECONDS);

        User user = User.builder().username("蜘蛛侠").password("zzx123").build();
        redisTemplate.opsForValue().set(user.getUsername(), user);
        user = User.builder().username("钢铁侠").password("gtx4125").build();
        redisTemplate.opsForValue().set(user.getUsername(), user);
        user = User.builder().username("美国队长").password("mgdz56325").build();
        redisTemplate.opsForValue().set(user.getUsername(), user);

        Assert.assertEquals("zzx123", redisTemplate.opsForValue().get("蜘蛛侠").getPassword());
        Assert.assertEquals("gtx4125", redisTemplate.opsForValue().get("钢铁侠").getPassword());
        Assert.assertEquals("mgdz56325", redisTemplate.opsForValue().get("美国队长").getPassword());
    }
}
