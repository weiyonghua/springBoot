package org.weiyonghua.service;

import org.weiyonghua.domain.User;

import java.util.List;

/**
 * Created by weiyonghua on 2017/3/22.
 */
public interface UserService {
    /**
     * Save user.
     *
     * @param user the user
     */
    void saveUser(User user);

    /**
     * Delete by username.
     *
     * @param username the username
     */
    void deleteByUsername(String username);

    /**
     * List user list.
     *
     * @return the list
     */
    List<User> listUser();

    /**
     * Delete all users.
     */
    void deleteAllUsers();

    User getUserByUsername(String username);
}
