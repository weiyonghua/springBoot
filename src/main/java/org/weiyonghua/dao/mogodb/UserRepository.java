package org.weiyonghua.dao.mogodb;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.weiyonghua.domain.User;

/**
 * Created by weiyonghua on 2017/3/27.
 */
public interface UserRepository extends MongoRepository<User, Long> {
    User findByUsername(String username);
}
