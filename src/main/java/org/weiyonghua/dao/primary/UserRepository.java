package org.weiyonghua.dao.primary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.weiyonghua.domain.User;

/**
 * Created by weiyonghua on 2017/3/23.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    @Query("from User u where u.username=:username")
    User findUser(@Param("username") String username);
}
