package org.weiyonghua.dao.secondary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.weiyonghua.domain.Message;

/**
 * Created by weiyonghua on 2017/3/27.
 */
public interface MessageRepository extends JpaRepository<Message, Long> {

}
