package org.weiyonghua.domain;

import lombok.*;

import javax.persistence.*;

/**
 * Created by weiyonghua on 2017/3/27.
 */
@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @Builder
@Table(name = "t_message")
public class Message {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long messageId;

    @Column(name = "message_content")
    private String messageContent;
}
