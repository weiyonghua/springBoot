package org.weiyonghua.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by weiyonghua on 2017/3/23.
 */
@Getter @Setter @Data @AllArgsConstructor @NoArgsConstructor
@Builder
@Entity
@Table(name = "t_user")
public class User implements Serializable {
    private static final long serialVersionUID = -6640661462455396108L;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long userId;
    @Column(name = "username", nullable = false)
    private String username;
    @Column(name = "password", nullable = false)
    private String password;
}
